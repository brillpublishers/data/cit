'''Handle browsing for the CIT
'''

cache = {}

def get_term(term, dokids=False, callback=None):

    def handle_data(response):
        if response.status == 200:
            return response.json()
        raise Exception('Bogus Data')

    def handle_json(data):        
        data = dict(data)
        cache[data['term']] = data
        if dokids:
            for k in data.get('kids', []):
                get_term(k)
        if callback:
            history.pushState({term: term}, "", term)
            setTimeout(lambda x: callback(term), 500)
            

    def get_fail(error):
        console.log(error)

    fetch('/api/term/'+term).then(handle_data).then(handle_json).catch(get_fail)

def update_ui(term):
    main = document.getElementById('main')

    data = cache.get(term)
    if not data:
        main.innerHTML = 'Term not in cache ' + term
        return
    
    main.innerHTML = ''
    home = document.createElement('img')
    home.setAttribute('src', 'home.svg')
    home.setAttribute('x-term', 'CIT')
    home.setAttribute('class', 'node')
    home.addEventListener("click", on_main_click)
    main.appendChild(home)
    
    for p in data['path']:
        pad = cache.get(p)
        path = document.createElement('div')
        path.setAttribute('class', 'path')
        path.setAttribute('x-term', p)
        path.addEventListener("click", on_main_click)
        if not pad:
            path.innerHTML = p
        else:
            path.innerHTML = pad['term'] + ' ' + (pad['term_en'] or '')
        main.appendChild(path)


    n = document.createElement('div')
    english = data['term_en']
    if english == None:
        english = ''
        
    n.innerHTML = '<h2 style="display: inline">' + data['term'] + ' &nbsp;' + english + '</h2>'
    main.appendChild(n)

    kinders = data.get('kids', [])

    for k in data.get('kids', []):
        kid = cache.get(k)
        if not kid:
            kid = {'term':k, 'term_en':'&middot;'}
        tmp = document.createElement('div')
        tmp.setAttribute('x-term', k)
        tmp.setAttribute('class', 'node')
        tmp.innerHTML = kid['term'] + ' ' + (kid['term_en'] or '')
        tmp.addEventListener("click", on_main_click)        
        main.appendChild(tmp)
    if data['broader']:
        up = document.createElement('img')
        up.setAttribute('src', 'up.svg')
        up.setAttribute('class', 'node')
        up.setAttribute('x-term', data['broader'])
        up.addEventListener("click", on_main_click)
        main.appendChild(up)


def on_main_click(event):
    event.preventDefault()
    term = event.target.getAttribute('x-term')
    get_term(term, True, update_ui)


get_term('CIT', True, update_ui)

# document.getElementById('main').addEventListener("click", on_main_click)
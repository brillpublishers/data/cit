import sys
import xml.etree.ElementTree as ET
import json
import sqlite3


def getpath(obj):
    parent = obj.get("broader")
    if parent:
        for p in getpath(data.get(parent)):
            yield p
        yield parent


def gettit(elem, xpath, dest, field):
    d = elem.find(xpath)
    if d is not None:
        dest[field] = d.text


# Note, parsing file: CIT export_inOrder_04.03.2019.xml 20190312 gives error in XML on lines 781763 and 781765 encountering character  '\x02' embedded in file.
# Looks like all the source have this so do a search and replace to fix it.
filecontents = open(sys.argv[1]).read().replace("\x02", "")
doc = ET.fromstring(filecontents)

data = {}
seq = 0
for entry in doc.findall(".//mus_ixthes"):
    seq += 1
    tmp = {"seq": seq}
    tmp["term"] = entry.find(".//mus_auth_thes_term").text
    gettit(entry, "mus_auth_thes_b_term", tmp, "broader")
    gettit(entry, "mus_auth_thes_term_discrim", tmp, "discrim")
    gettit(entry, "mus_auth_thes_scope_note", tmp, "scopenote")
    gettit(entry, "mus_auth_thes_source", tmp, "source")

    # Find the _ elements under mus_auth_thes_a_terms_alias to extract the English display terms
    terms_alias = entry.find(".//mus_auth_thes_a_terms_alias")
    if terms_alias is None:
        continue

    for term in terms_alias.findall(".//_"):
        term_type = term.find(".//mus_auth_thes_a_terms_type_val")
        if term_type is None:
            continue
        term_val = term.find(".//mus_auth_thes_a_terms")
        if term_val is None:
            continue

        if term_type.text == "English display term":
            tmp["term_en"] = term_val.text
        elif term_type.text == "Pinyin":
            tmp["term_pinyin"] = term_val.text

    disc = tmp.get("discrim")
    if disc:
        tmpid = f"{tmp['term']} ({disc})"
    else:
        tmpid = tmp["term"]
    tmp["term"] = tmpid
    data[tmpid] = tmp

# And also run through all the data and update the children before pickling
for x in data.values():
    parent = data.get(x.get("broader"))
    if parent:
        parent.setdefault("kids", []).append(x["term"])

for x in data.values():
    x["path"] = list(getpath(x))

if "TBC" in data["CIT"]["kids"]:
    data["CIT"]["kids"].remove("TBC")

with open("CIT.dmp", "w") as OUT:
    for obj in data.values():
        for k, v in obj.items():
            if v:
                v = str(v)
                new_v = "\n".join([f" {vv}" for vv in v.split("\n")])
                OUT.write(f"{k.upper()}{new_v}\n")
        OUT.write("$\n")


def dump_to_db(data):
    # CREATE TABLE terms (term, body);
    # CREATE UNIQUE INDEX terms_term ON terms (term);
    # But it would also be good to write out the entries to a sqlite dbm, so that we can easily query them.
    db = sqlite3.connect("CIT.sqlite3")
    cursor = db.cursor()
    batch = [(k, json.dumps(v)) for k, v in data.items()]
    cursor.executemany("INSERT INTO terms VALUES (?, ?)", batch)
    db.commit()


dump_to_db(data)
